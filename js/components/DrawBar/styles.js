
export default {
  drawer: {
    backgroundColor: 'white',
  },
  bigImage: {
    height: 120,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  smallImage: {
    height: 120,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  smallImageSize: { height: 80, width: 80, borderRadius: 40 },
  textColor: { color: '#e93841' },
};
