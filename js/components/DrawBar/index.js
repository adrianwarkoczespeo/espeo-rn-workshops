import React from 'react';
import { Image, TouchableOpacity, Linking } from 'react-native';
import {
  Text,
  Container,
  List,
  ListItem,
  Content,
} from 'native-base';
import { connect } from 'react-redux';
import { closeDrawer } from '../../actions/drawer';
import styles from './styles';

const backgroundEspeo = require('../../../images/espeo.png');
const backgroundEspeoNinja = require('../../../images/espeo-ninja.png');

class DrawBar extends React.Component {
  static navigationOptions = {
    header: null,
  };
  navigate(data) {
    const { navigation } = this.props;
    return data.isExternal ? Linking.openURL(data.page) : navigation.navigate(data.page);
  }
  render() {
    const { routes } = this.props;
    return (
      <Container style={styles.drawer}>
        <Content>
          <Image
            source={backgroundEspeo}
            style={styles.bigImage}
          >
            <TouchableOpacity
              style={styles.smallImage}
            >
              <Image
                style={styles.smallImageSize}
                source={backgroundEspeoNinja}
              />
            </TouchableOpacity>
          </Image>
          <List
            dataArray={routes}
            renderRow={data => (
              <ListItem
                button
                onPress={() => this.navigate(data)}
              >
                <Text style={styles.textColor}>{data.name}</Text>
              </ListItem>
              )}
          />
        </Content>
      </Container>
    );
  }
}
const mapStateToProps = state => ({
  isDrawerActive: state.drawer.isDrawerActive,
  routes: state.drawer.routes,
});

const DrawBarPackage = connect(mapStateToProps, null)(DrawBar);
export default DrawBarPackage;
