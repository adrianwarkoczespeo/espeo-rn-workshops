import React, { Component } from 'react';
import { Image } from 'react-native';
import {
  Container,
  Content,
  Button,
  View,
  Text,
} from 'native-base';
import styles from './styles';

const background = require('../../../images/espeo.png');

class WelcomePage extends Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <View style={styles.container}>
          <Content>
            <Image source={background} style={styles.shadow}>
              <View style={styles.bg}>
                <Button
                  style={styles.btn}
                  onPress={() => navigation.navigate('Home')}
                >
                  <Text>Enter React Native Espeo Workshops #1</Text>
                </Button>
              </View>
            </Image>
          </Content>
        </View>
      </Container>
    );
  }
}

export default WelcomePage;
