import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Drawer,
} from 'native-base';
import { Grid, Row } from 'react-native-easy-grid';
import DrawBar from '../DrawBar';
import { setIndex } from '../../actions/list';
import { openDrawer, closeDrawer } from '../../actions/drawer';
import styles from './styles';

class Home extends Component {
  static navigationOptions = {
    header: null,
  };
  static propTypes = {
    list: React.PropTypes.arrayOf(React.PropTypes.object),
  };
  constructor(props) {
    super(props);
    this.toggleDrawer = this.toggleDrawer.bind(this);
  }

  toggleDrawer() {
    const { openDrawerAction, closeDrawerAction, isDrawerActive } = this.props;
    if (isDrawerActive) {
      closeDrawerAction();
      this.drawer._root.close();
    } else {
      openDrawerAction();
      this.drawer._root.open();
    }
  }

  render() {
    const { list, navigation } = this.props;
    return (
      <Drawer
        ref={(ref) => { this.drawer = ref; }}
        content={<DrawBar navigation={navigation} />}
      >
        <Container style={styles.container}>
          <Header style={{ zIndex: 99 }}>
            <Left>
              <Button
                transparent
                onPress={() => navigation.navigate('Login')}
              >
                <Icon active name="power" />
              </Button>
            </Left>

            <Body>
              <Title>Send Mail</Title>
            </Body>

            <Right>
              <Button
                transparent
                onPress={this.toggleDrawer}
              >
                <Icon active name="menu" />
              </Button>
            </Right>
          </Header>
          <Content>
            <Grid style={styles.mt}>
              {list.map((item, i) => (
                <Row key={i}>
                  <TouchableOpacity
                    style={styles.row}
                    onPress={() =>
                    navigation.navigate('NewEmail')}
                  >
                    <Text style={styles.text}>{item.name}</Text>
                  </TouchableOpacity>
                </Row>
            ))}
            </Grid>
          </Content>
        </Container>
      </Drawer>
    );
  }
}

function bindAction(dispatch) {
  return {
    setIndex: index => dispatch(setIndex(index)),
    openDrawerAction: () => dispatch(openDrawer()),
    closeDrawerAction: () => dispatch(closeDrawer()),
  };
}
const mapStateToProps = state => ({
  list: state.list.list,
  isDrawerActive: state.drawer.isDrawerActive,
});

const HomeSwagger = connect(mapStateToProps, bindAction)(Home);
export default HomeSwagger;
