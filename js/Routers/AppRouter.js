import { StackNavigator } from 'react-navigation';
import WelcomePage from '../components/welcomePage';
import Home from '../components/home/';
import Received from '../components/received';
import Sent from '../components/sent';
import NewEmail from '../components/new';

export default (StackNav = StackNavigator({
  Login: { screen: WelcomePage },
  Home: { screen: Home },
  Received: { screen: Received },
  Sent: { screen: Sent },
  NewEmail: { screen: NewEmail },
}));
