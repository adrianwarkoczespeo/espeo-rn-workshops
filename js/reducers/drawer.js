
import { OPEN_DRAWER, CLOSE_DRAWER } from '../actions/drawer';

const initialState = {
  isDrawerActive: false,
  drawerDisabled: true,
  routes: [
  { name: 'Sent', page: 'Sent', isExternal: false },
  { name: 'Recieved', page: 'Received', isExternal: false },
  { name: 'Join Us', page: 'https://espeo.eu/career/', isExternal: true },
  ],
};

export default function (state = initialState, action) {
  if (action.type === OPEN_DRAWER) {
    return {
      ...state,
      isDrawerActive: true,
    };
  }

  if (action.type === CLOSE_DRAWER) {
    return {
      ...state,
      isDrawerActive: false,
    };
  }

  return state;
}
