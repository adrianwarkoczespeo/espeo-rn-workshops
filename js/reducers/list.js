import { SET_INDEX } from '../actions/list';

const initialState = {
  list: [
    { name: 'Default...', topic: '' },
    { name: 'Welcome Mail', topic: 'New cooworker' },
    { name: 'End of cooperation', topic: 'End of cooperation' },
    { name: 'Out of the office', topic: 'Sorry, I’m Out of the Office' },
  ],
  selectedIndex: undefined,
};

export default function (state = initialState, action) {
  if (action.type === SET_INDEX) {
    return {
      ...state,
      selectedIndex: action.payload,
    };
  }
  return state;
}
